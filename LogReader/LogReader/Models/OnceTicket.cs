﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogReader.Models
{
    public class OnceTicket
    {
        public string ObuID { get; set; }
        public DateTime CheckDate { get; set; }
        public DateTime CheckTime { get; set; }
        public int VehicleClassID { get; set; }
        public int LoginID { get; set; }
        public int LaneID { get; set; }
        public int ShiftID { get; set; }
        public int StationID { get; set; }
        public string RegisPlatNumber { get; set; }
        public string RecogPlatNumber { get; set; }
        public int ImageID { get; set; }
        public DateTime Checker { get; set; }
        public int TicketCode { get; set; }
        public string Etag { get; set; }
        public int F0 { get; set; }
        public int F1 { get; set; }
        public int Result { get; set; }
    }
}
