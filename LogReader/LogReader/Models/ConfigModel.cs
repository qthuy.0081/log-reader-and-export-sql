﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogReader.Models
{
    public class ConfigModel
    {
        public ConfigModel() { }
        public ConfigModel(string log, string sql)
        {
            this.Log = log;
            this.Sql = sql;
        }

        public string Log { get; set; }
        public string Sql { get; set; }
    }
}
