﻿using LogReader.Commons;
using LogReader.Models;
using LogReader.Views;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace LogReader.ViewModels
{
    public class ConfigViewModel : BaseViewModel
    {
        #region fields and props
        private string _keyword;
        public string Keyword
        {
            get => _keyword;
            set
            {
                if(_keyword != value)
                {
                    _keyword = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _attribute;
        public string Attribute
        {
            get => _attribute;
            set
            {
                if(_attribute != value)
                {
                    _attribute = value;
                    OnPropertyChanged();
                    CanAdd();
                }
            }
        }
        private string _value;
        public string Value
        {
            get => _value;
            set
            {
                if(_value != value)
                {
                    _value = value;
                    OnPropertyChanged();
                }
            }
        }
        private ObservableCollection<ConfigModel> _displayData = new ObservableCollection<ConfigModel>();
        public ObservableCollection<ConfigModel> DisplayData
        {
            get => _displayData;
            set
            {
                if(_displayData != value)
                {
                    _displayData = value;
                    OnPropertyChanged();
                }
            }
        }
        private ConfigModel _selectedModel = new ConfigModel();
        public ConfigModel SelectedModel 
        {
            get => _selectedModel;
            set
            {
                if(_selectedModel != value)
                {
                    _selectedModel = value;
                    OnPropertyChanged();
                }
            }
        }

        private Dictionary<string, Dictionary<string, List<string>>> _keywordDict;
        public Dictionary<string, Dictionary<string, List<string>>> KeywordDict
        {
            get => _keywordDict;
            
            set
            {
                if(_keywordDict != value)
                {
                    _keywordDict = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _selectedKeyword;
        public string SelectedKeyword 
        {
            get => _selectedKeyword;
            set
            {
                if(_selectedKeyword != value)
                {
                    _selectedKeyword = value;
                    DisplayData = ConvertData();
                    OnPropertyChanged();
                }
            }
        }


        private Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();

        #endregion
        //Construct
        public ConfigViewModel()
        {
            DataParser.MappingValues.Load();
            KeywordDict = DataParser.MappingValues.mapping;
        }
        #region methods
        private void AddAttribute()
        {
            try
            {
                List<string> values = Value.Trim().Split(',').ToList();
                dictionary.Add(Attribute, values);
                DisplayData.Add(new ConfigModel(Attribute.Trim(), Value.Trim()));
            }
            catch (Exception e)
            {
                MessageBox.Show("Duplicated Attribute...!");
            }
        }
        private void DeleteAtrribute()
        {
            try
            {
                DisplayData.Remove(SelectedModel);
            }
            catch (Exception e)
            {

            }
        }
        private void Save()
        {
            try
            {
                //DataParser.MappingValues.mapping.Add(Keyword,dictionary);
                //KeywordDict[SelectedKeyword] = ConvertBack();
                DataParser.MappingValues.mapping = KeywordDict;
                if(!DataParser.MappingValues.Save())
                {
                    return;
                }
                MessageBox.Show("Save successfully");
            }
            catch (Exception e)
            {
                MessageBox.Show("Duplicated keyword");
            }
        }

        private ObservableCollection<ConfigModel> ConvertData ()
        {
            dictionary = KeywordDict[SelectedKeyword];
            ObservableCollection<ConfigModel> keywordModels = new ObservableCollection<ConfigModel>();
            foreach(KeyValuePair<string, List<string>> i in dictionary)
            {
                ConfigModel keywordProp = new ConfigModel();
                keywordProp.Log = i.Key;
                keywordProp.Sql = String.Join(", ", i.Value.ToArray());
                keywordModels.Add(keywordProp);
            }
            return keywordModels;
        }


        private Dictionary<string, List<string>> ConvertBack()
        {
            Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();
            foreach(ConfigModel model in DisplayData)
            {
                string key = model.Log;
                List<string> values = model.Sql.Trim().Split(',').ToList();
                dict.Add(key, values);
            }
            return dict;
        }
        private void OpenAddKeyword()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog() { Multiselect = true };
            if (openFileDialog.ShowDialog() == true)
            {
               foreach(var file in openFileDialog.FileNames)
               {
                    Dictionary<string, Dictionary<string, List<string>>> newKeywords = new Dictionary<string, Dictionary<string, List<string>>>();
                    newKeywords = DataParser.MappingValues.LoadNewKeyWords(file);
                    foreach(KeyValuePair<string, Dictionary<string, List<string>>> keyword in newKeywords)
                    {
                        if(KeywordDict.ContainsKey(keyword.Key))
                        {
                            KeywordDict[keyword.Key] = keyword.Value;
                        }
                        else
                        {
                            KeywordDict.Add(keyword.Key,keyword.Value);
                        }
                    }
               }
            }
            KeywordDict = new Dictionary<string, Dictionary<string, List<string>>>(KeywordDict);
        }
        
        private bool CanAdd()
        {
            if(String.IsNullOrEmpty(Attribute))
            {
                return false;
            }
            return char.IsLetter(Attribute[0]);
        }

        #endregion
        #region commands
        private ICommand _addAtrCmd;
        public ICommand addAtrCmd => _addAtrCmd ?? (_addAtrCmd = new RelayCommand(param =>AddAttribute(),p => CanAdd()));

        private ICommand _deleteAtrCmd;
        public ICommand deleteAtrCmd => _deleteAtrCmd ?? (_deleteAtrCmd = new RelayCommand(param => DeleteAtrribute()));

        private ICommand _saveCmd;
        public ICommand saveCmd => _saveCmd ?? (_saveCmd = new RelayCommand(param => Save()));

        private ICommand _openAddkewordCmd;
        public ICommand openAddkewordCmd => _openAddkewordCmd ?? (_openAddkewordCmd = new RelayCommand(param => OpenAddKeyword()));

        #endregion
    }
}
