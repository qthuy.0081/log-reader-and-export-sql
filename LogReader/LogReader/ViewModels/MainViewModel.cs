﻿namespace LogReader.ViewModels
{
    using LogReader.Commons;
    using LogReader.Views;
    using Microsoft.Win32;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Defines the <see cref="MainViewModel" />.
    /// </summary>
    public class MainViewModel : BaseViewModel
    {
        /// <summary>
        /// Defines the _openFileDialog.
        /// </summary>
        private OpenFileDialog _openFileDialog;

        /// <summary>
        /// Gets or sets the OpenFileDialog.
        /// </summary>
        public OpenFileDialog OpenFileDialog
        {
            get => _openFileDialog;
            set
            {
                if (_openFileDialog != value)
                {
                    _openFileDialog = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Defines the _myTable.
        /// </summary>
        private DataTable _myTable;

        /// <summary>
        /// Gets or sets the MyTable.
        /// </summary>
        public DataTable MyTable
        {
            get => _myTable;
            set
            {
                if (_myTable != value)
                {
                    _myTable = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Defines the _keyword.
        /// </summary>
        private Dictionary<string, Dictionary<string, List<string>>> _keywords = new Dictionary<string, Dictionary<string, List<string>>>();

        /// <summary>
        /// Gets or sets the Keyword.
        /// </summary>
        public Dictionary<string, Dictionary<string, List<string>>> Keywords
        {
            get => _keywords;
            set
            {
                if (_keywords != value)
                {
                    _keywords = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _selectedKeyword;
        public string SelectedKeyword
        {
            get => _selectedKeyword;
            set
            {
                if (_selectedKeyword != value)
                {
                    _selectedKeyword = value;
                    OnPropertyChanged();
                    Dictionary<string, List<string>> keyword = Keywords[SelectedKeyword];
                    List<string> mapping;
                    keyword.TryGetValue("tablename", out mapping);
                    TableName = mapping.FirstOrDefault();
                }
            }
        }
        private string _tableName;
        public string TableName 
        {
            get => _tableName;
            set
            {
                if(_tableName != value)
                {
                    _tableName = value;
                    OnPropertyChanged();
                }
            }
        }

        List<Dictionary<string, string>> jobjects = new List<Dictionary<string, string>>();
        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
            //MyTable = CreateDataTable();
            DataParser.MappingValues.Load();
            Keywords = DataParser.MappingValues.mapping;
        }

        /// <summary>
        /// The browseFileCmd.
        /// </summary>
        private void BrowseFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog() { Multiselect = true };
            if (openFileDialog.ShowDialog() == true)
            {
                OpenFileDialog = openFileDialog;
            }
        }

        /// <summary>
        /// The ReadLog.
        /// </summary>
        private void ReadLog()
        {
            if (_openFileDialog == null)
            {
                return;
            }
            jobjects = new List<Dictionary<string, string>>();
            DataTable result = new DataTable();
            foreach (var item in _openFileDialog.FileNames)
            {
                Thread thread = new Thread(() =>
                {
                    StreamReader streamReader = new StreamReader(item);
                    string line = streamReader.ReadLine();
                    while (line != null)
                    {
                        Dictionary<string, string> jobject = DataParser.RowParse(line, SelectedKeyword);
                        if (jobject != null)
                        {
                            var row = result.NewRow();

                            for (int i = 0; i < jobject.Count; i++)
                            {
                                string columnName = jobject.Keys.ToArray()[i];
                                string value = jobject[columnName];
                                
                                if (!result.Columns.Contains(columnName))
                                {
                                    result.Columns.Add(columnName);
                                }
                                row[columnName] = value;
                            }
                            // Add row to data grid
                            result.Rows.Add(row);
                            //add to list
                            jobjects.Add(jobject);
                        }
                        line = streamReader.ReadLine();
                    }
                    streamReader.Close();
                });
                thread.Start();
                thread.Join();
                MyTable = result;
            }
        }

        private void ExportSQL()
        {
            if(string.IsNullOrEmpty(TableName))
            {
                MessageBox.Show("Please input table name...!");
                return;
            }
            if(jobjects.Count > 0)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "SQL|*.sql";
                if(saveFileDialog.ShowDialog() == true)
                {
                    StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName);
                    foreach(var item in jobjects)
                    {
                        string sql = "";
                        //if (Keyword == "FinanceTicket" || Keyword == "ForceOneTicket")
                            sql = DataParser.CreateInsertSQlEx(TableName, item, SelectedKeyword);
                        //else
                        //    sql = DataParser.CreateInsertSQl(TableName, item);
                        streamWriter.WriteLine(sql);
                    }
                    streamWriter.Close();
                    MessageBox.Show("Export Successful!");
                }
            }
        }
        private void OpenConfig()
        {
            ConfigWindow window = new ConfigWindow();
            window.ShowDialog();
            DataParser.MappingValues.Load();
            Keywords = DataParser.MappingValues.mapping;
        }
        /// <summary>
        /// Defines the _browseFileCmd.
        /// </summary>
        private ICommand _browseFileCmd;

        /// <summary>
        /// Gets the browseFileCmd.
        /// </summary>
        public ICommand browseFileCmd => _browseFileCmd ?? (_browseFileCmd = new RelayCommand(param => BrowseFile()));

        /// <summary>
        /// Defines the _readLog.
        /// </summary>
        private ICommand _readLog;

        /// <summary>
        /// Gets the readLog.
        /// </summary>
        public ICommand readLog => _readLog ?? (_readLog = new RelayCommand(param => ReadLog()));

        private ICommand _exportSQL;
        public ICommand exportSQL => _exportSQL ?? (_exportSQL = new RelayCommand(param => ExportSQL()));

        private ICommand _openCofigCmd;
        public ICommand openConfigCmd => _openCofigCmd ?? (_openCofigCmd = new RelayCommand(param =>OpenConfig()));
    }
}
