﻿using LogReader.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace LogReader.ViewModels
{
    public class AddKeywordVM : BaseViewModel
    {
        public string newKeyword="";

        private string _keyword;
        public string Keyword
        {
            get => _keyword;
            set
            {
                if(_keyword != value)
                {
                    _keyword = value;
                    OnPropertyChanged();
                }
            }
        }

        private void AddKeyword()
        {
            if(!DataParser.MappingValues.mapping.ContainsKey(Keyword))
            {
                newKeyword = Keyword;
                MessageBox.Show("Add successfully");
                CloseWindow();
            }
            else
            {
                MessageBox.Show("Duplicated keyword");
            }
        }

        private ICommand _addKeywordCmd;
        public ICommand addKeywordCmd => _addKeywordCmd ?? (_addKeywordCmd = new RelayCommand(param => AddKeyword()));
    }
}
