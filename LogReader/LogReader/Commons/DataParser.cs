﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogReader.Commons
{
    public class DataParser
    {
        public static MappingValues MappingValues = new MappingValues();
        public static Dictionary<string, string> RowParse(string raw, string keyword)
        {
            try
            {
                string[] rawDatas = raw.Split(new char[] { '[', ']' });

                if (rawDatas.Length > 2)
                {
                    string rawData = rawDatas[1];
                    if (rawData.StartsWith(keyword) || keyword == "")
                    {
                        rawData = rawData.Substring(rawData.IndexOf(" - ") + " - ".Length);

                        Dictionary<string, string> dataObject = new Dictionary<string, string>();
                        //split by "|"
                        string[] datas = rawData.Split('|');
                        for (int i = 0; i < datas.Length; i++)
                        {
                            //split by ":"
                            string data = datas[i];
                            int offset = data.IndexOf(':');
                            string field = data.Substring(0, offset);
                            string value = data.Substring(offset + 1, data.Length - offset - 1);
                            dataObject.Add(field, value);
                        }
                        return dataObject;
                    }
                }
            }
            catch { }
            return null;
        }

        public static string CreateInsertSQl(string tableName, Dictionary<string, string> jobject)
        {
            try
            {
                string keys = "";
                string values = "";
                string[] props = jobject.Keys.ToArray();
                for (int i = 0; i < props.Length; i++)
                {
                    if (props[i].ToUpper() != "RESULT")
                    {
                        keys = i == 0 ? keys + props[i] : keys + "," + props[i];
                        values = i == 0 ? values + $"'{jobject[props[i]]}'" : values + $",'{jobject[props[i]]}'";
                    }
                }
                string sql = $"insert into {tableName} ({keys}) values ({values})";
                sql = sql.Replace("RegisPlat", "RegisPlate");
                sql = sql.Replace("RecogPlat", "RecogPlate");
                sql = sql.Replace("PlatType", "PlateType");
                return sql;
            }
            catch { }
            return "";
        }
        public static string CreateInsertSQlEx(string tableName, Dictionary<string, string> jobject, string keyword)
        {
            try
            {
                Dictionary<string, List<string>> mapping;
                MappingValues.mapping.TryGetValue(keyword, out mapping);
                string keys = "";
                string values = "";
                string[] props = jobject.Keys.ToArray();
                bool fisrt = false;
                for (int i = 0; i < props.Length; i++)
                {
                    if (props[i].ToUpper() != "RESULT")
                    {
                        string key = props[i];
                        string value = jobject[props[i]];

                        List<string> actualColumns;

                        if (mapping.TryGetValue(key, out actualColumns))
                        {
                            foreach (var item in actualColumns)
                            {
                                keys = fisrt == false ? keys + item : keys + "," + item;
                                if (key.EndsWith("Date"))
                                {
                                    value = DateTime.Parse(value).ToString("yyyy-MM-dd");
                                }
                                values = fisrt == false ? values + $"'{value}'" : values + $",'{value}'";
                                if (fisrt == false)
                                    fisrt = true;
                            }
                        }
                        else
                        {
                            //keys = i == 0 ? keys + key : keys + "," + key;
                            //values = i == 0 ? values + $"'{value}'" : values + $",'{value}'";
                        }
                    }
                }
                string sql = $"insert into {tableName} ({keys}) values ({values})";
                sql = sql.Replace("RegisPlat", "RegisPlate");
                sql = sql.Replace("RecogPlat", "RecogPlate");
                sql = sql.Replace("PlatType", "PlateType");
                return sql;
            }
            catch (Exception ex) { }
            return "";
        }
    }
}
