﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogReader.Commons
{
    public static class Utils
    {  
        public static bool CheckValidKeyword(string keyword)
        {

            return char.IsLetter(keyword[0]);
        }
    }
}
