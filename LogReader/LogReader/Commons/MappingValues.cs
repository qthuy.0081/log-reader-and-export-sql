﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;

namespace LogReader.Commons
{
    public class MappingValues
    {
        public Dictionary<string, Dictionary<string, List<string>>> mapping = new Dictionary<string, Dictionary<string, List<string>>>();
        private const string configFile = "config.xml";
        public  MappingValues()
        {
            ////BTC
            //Dictionary<string, List<string>> forceFinanceTicketMapping = new Dictionary<string, List<string>>();
            //forceFinanceTicketMapping.Add("CheckDate", new List<string>() { "Ngayqua" });
            //forceFinanceTicketMapping.Add("CheckTime", new List<string>() { "Gioqua" });
            //forceFinanceTicketMapping.Add("LaneID", new List<string>() { "MSLane" });
            //forceFinanceTicketMapping.Add("VehicleClassID", new List<string>() { "Loaixe" });
            //forceFinanceTicketMapping.Add("LoginID", new List<string>() { "Login" });
            //forceFinanceTicketMapping.Add("ShiftID", new List<string>() { "Ca" });
            //forceFinanceTicketMapping.Add("Checker", new List<string>() { "Checker" });
            //forceFinanceTicketMapping.Add("F0", new List<string>() { "F0" });
            //forceFinanceTicketMapping.Add("F1", new List<string>() { "F1" });
            //forceFinanceTicketMapping.Add("F2", new List<string>() { "F2" });
            //forceFinanceTicketMapping.Add("RecogPlatNumber", new List<string>() { "Soxe_ND" });
            //forceFinanceTicketMapping.Add("ImageID", new List<string>() { "ImageID", "ID" });
            //forceFinanceTicketMapping.Add("StationID", new List<string>() { "Tram" });
            //mapping.Add("FinanceTicket", forceFinanceTicketMapping);

            ////ForceOpen
            //Dictionary<string, List<string>> forceOneTicketMapping = new Dictionary<string, List<string>>();
            //forceOneTicketMapping.Add("CheckDate", new List<string>() { "NGAYMO", "NGAYDONG" });
            //forceOneTicketMapping.Add("CheckTime", new List<string>() { "GIOMO", "GIODONG" });
            //forceOneTicketMapping.Add("LaneID", new List<string>() { "MSLane" });
            //forceOneTicketMapping.Add("LoginID", new List<string>() { "Login" });
            //forceOneTicketMapping.Add("ObuID", new List<string>() { "TID" });
            //forceOneTicketMapping.Add("ShiftID", new List<string>() { "Ca" });
            //forceOneTicketMapping.Add("Checker", new List<string>() { "Checker" });
            //forceOneTicketMapping.Add("F0", new List<string>() { "F0" });
            //forceOneTicketMapping.Add("F1", new List<string>() { "F1" });
            //forceOneTicketMapping.Add("F2", new List<string>() { "F2" });
            //forceOneTicketMapping.Add("RecogPlatNumber", new List<string>() { "Soxe_ND" });
            //forceOneTicketMapping.Add("ImageID", new List<string>() { "ImageID" });
            //forceOneTicketMapping.Add("StationID", new List<string>() { "Tram" });
            //mapping.Add("ForceOneTicket", forceOneTicketMapping);
            Load();
        }
        public bool Save()
        {
            try
            {
                XElement root = new XElement("root");
                foreach (KeyValuePair<string, Dictionary<string, List<string>>> i in mapping)
                {
                    XElement child = new XElement(i.Key, from k in i.Value select new XElement(k.Key, String.Join("-",k.Value.ToArray())));
                    root.Add(child);
                }
                root.Save(configFile);
                return true;

            } catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return false;
            }
        }

        public bool Load()
        {
            mapping = new Dictionary<string, Dictionary<string, List<string>>>();
            try
            {
                if (File.Exists(configFile))
                {
                    XElement root2 = XElement.Load("config.xml");
                    mapping = new Dictionary<string, Dictionary<string, List<string>>>();
                    foreach (XElement el in root2.Elements())
                    {
                        Dictionary<string, List<string>> forceFinanceTicketMapping2 = new Dictionary<string, List<string>>();
                        foreach (XElement t in el.Elements())
                        {
                            forceFinanceTicketMapping2.Add(t.Name.LocalName, t.Value.Split('-').ToList());
                        }
                        mapping.Add(el.Name.LocalName, forceFinanceTicketMapping2);
                    }
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public Dictionary<string, Dictionary<string, List<string>>> LoadNewKeyWords(string filePath)
        {
            Dictionary<string, Dictionary<string, List<string>>> newKeywords = new Dictionary<string, Dictionary<string, List<string>>>();
            try
            {
                if (File.Exists(filePath))
                {
                    XElement root2 = XElement.Load(filePath);
                    foreach (XElement el in root2.Elements())
                    {
                        Dictionary<string, List<string>> keyWord_Values = new Dictionary<string, List<string>>();
                        foreach (XElement t in el.Elements())
                        {
                            keyWord_Values.Add(t.Name.LocalName, t.Value.Split('-').ToList());
                        }
                        newKeywords.Add(el.Name.LocalName, keyWord_Values);
                    }
                }
            }
            catch (Exception e)
            {

            }
            return newKeywords;
        }
    }
}
